import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {TableConfiguration} from '@universis/ngx-tables';

export class ValidateResultsTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./validate-results-table.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`packages/registrar/src/app/departments/components/validate-preview-results/validate-results-table.config.list.json`);
        });
    }
}


export class ValidateResultsDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`packages/registrar/src/app/departments/components/validate-preview-results/validate-results-table.search.list.json`);
    }
}
