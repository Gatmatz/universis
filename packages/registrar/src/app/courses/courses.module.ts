import { CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesSharedModule } from './courses.shared';
import { CoursesRoutingModule } from './courses.routing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { CoursesHomeComponent } from './components/courses-home/courses-home.component';
import { CoursesTableComponent } from './components/courses-table/courses-table.component';
import { TablesModule } from '@universis/ngx-tables';
import { CoursesRootComponent } from './components/courses-root/courses-root.component';
import { CoursesPreviewGeneralComponent } from './components/dashboard/courses-preview-general/courses-preview-general.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@universis/common';
import { ClassesSharedModule } from '../classes/classes.shared';
import { CoursesPreviewClassesComponent } from './components/dashboard/courses-preview-classes/courses-preview-classes.component';
import { ElementsModule } from '../elements/elements.module';
// tslint:disable-next-line:max-line-length
import { CoursesPreviewCourseDetailsFormComponent } from './components/dashboard/courses-preview-general/courses-preview-course-details-form.component';
import { CoursesOverviewComponent } from './components/dashboard/courses-overview/courses-overview.component';
import { CoursesExamsComponent } from './components/dashboard/courses-exams/courses-exams.component';
import { StudyProgramsSharedModule } from '../study-programs/study-programs.shared';
import { CoursesStudyProgramsComponent } from './components/dashboard/courses-study-programs/courses-study-programs.component';
import { ExamsSharedModule } from '../exams/exams.shared';
import { CoursesAdvancedTableSearchComponent } from './components/courses-table/courses-advanced-table-search.component';
// tslint:disable-next-line:max-line-length
import { CoursesClassesAdvancedTableSearchComponent } from './components/dashboard/courses-preview-classes/courses-classes-advanced-table-search.component';
import {MostModule} from '@themost/angular';
import { RegistrarSharedModule } from '../registrar-shared/registrar-shared.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import {RouterModalModule} from '@universis/common/routing';
import {CoursesOverviePartsComponent} from './components/dashboard/courses-overview/courses-overview-parts/courses-overview-parts.component';
import {CoursesStudentsComponent} from './components/dashboard/courses-students/courses-students.component';
import {CourseUpdateResultsComponent} from './components/dashboard/course-update-results/course-update-results.component';
import { ChangeCourseGradeScaleComponent } from './components/dashboard/courses-overview/change-course-grade-scale/change-course-grade-scale/change-course-grade-scale.component';
import { AdvancedFormsModule } from '@universis/forms';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoursesSharedModule,
    CoursesRoutingModule,
    ClassesSharedModule,
    StudyProgramsSharedModule,
    ExamsSharedModule,
    TablesModule,
    SharedModule,
    FormsModule,
    ElementsModule,
    MostModule,
    RegistrarSharedModule,
    RouterModalModule,
    AdvancedFormsModule
  ],
  declarations: [CoursesHomeComponent,
    CoursesTableComponent,
    CoursesRootComponent,
    CoursesPreviewGeneralComponent,
    CoursesPreviewClassesComponent,
    CoursesPreviewCourseDetailsFormComponent,
    CoursesOverviewComponent,
    CoursesExamsComponent,
    CoursesStudyProgramsComponent,
    CoursesAdvancedTableSearchComponent,
    CoursesClassesAdvancedTableSearchComponent,
    DashboardComponent,
    CoursesOverviePartsComponent,
    CoursesStudentsComponent,
    CourseUpdateResultsComponent,
    ChangeCourseGradeScaleComponent
  ],
  exports: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoursesModule { }
