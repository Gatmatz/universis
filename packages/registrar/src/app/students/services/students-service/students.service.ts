import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import {
  ConfigurationService,
  GradeScale,
  GradeScaleService,
} from '@universis/common';
import { NumberFormatter } from '@universis/number-format';

export declare interface GraduationGradeWrittenInWordsCalculationAttributes {
  studentStatus: string;
  graduationGrade: number;
  decimalDigits: number;
  graduationGradeScale: {
    id: number;
    scaleType: number;
  };
}

@Injectable({
  providedIn: 'root',
})
export class StudentsService {
  private _gradeScaleCache: Map<number, GradeScale> = new Map<
    number,
    GradeScale
  >();

  constructor(
    private readonly _context: AngularDataContext,
    private readonly _configurationService: ConfigurationService,
    private readonly _gradeScaleService: GradeScaleService
  ) {}

  async getProgramGroups(studentId: number) {
    return this._context
      .model('StudentProgramGroups')
      .where('student')
      .equal(studentId)
      .expand('student($expand=studentStatus)', 'programGroup')
      .take(-1)
      .getItems();
  }

  /**
   * Subscribe a student to a ptogramGroup
   *
   * @param studentId The target student id
   * @param groupId The target ProgramGroup id
   *
   */
  async addProgramGroup(studentId: number, groupId: number): Promise<any> {
    const group = await this._context
      .model('ProgramGroups')
      .where('id')
      .equal(groupId)
      .getItem();

    const exists = await this._context
      .model('StudentProgramGroups')
      .where('student')
      .equal(studentId)
      .and('programGroup')
      .equal(groupId)
      .select('id')
      .getItem();

    if (exists) {
      throw new Error('AlreadySubscribed');
    } else {
      const model = {
        object: studentId,
        groups: [group],
      };

      return this._context.model('UpdateStudentGroupActions').save(model);
    }
  }

  async calculateGraduationGradeWrittenInWords(
    calculationAttributes: GraduationGradeWrittenInWordsCalculationAttributes
  ): Promise<string | null> {
    try {
      // guard attributes
      if (calculationAttributes == null) {
        return Promise.resolve(null);
      }
      // if student is not graduated nor declared
      if (
        !['declared', 'graduated'].includes(calculationAttributes.studentStatus)
      ) {
        // exit (maybe consider throwing an error)
        return Promise.resolve(null);
      }
      // if any of the required attributes for the calculation is/are missing
      if (
        !(
          calculationAttributes.graduationGrade != null &&
          calculationAttributes.decimalDigits != null &&
          calculationAttributes.graduationGradeScale &&
          calculationAttributes.graduationGradeScale.id &&
          calculationAttributes.graduationGradeScale.scaleType != null
        )
      ) {
        console.warn(
          'Missing a required attribute for graduationGradeWrittenInWords calculation.'
        );
        // exit (maybe consider throwing an error)
        return Promise.resolve(null);
      }
      // get program decimal digits
      const decimalDigits: number = calculationAttributes.decimalDigits;
      // get program grade scale type
      const scaleType: number =
        calculationAttributes.graduationGradeScale.scaleType;
      // get current locale
      const locale: string = this._configurationService.currentLocale;
      // create number formatter
      const formatter: NumberFormatter = new NumberFormatter();
      // prepare decimal seperator
      const decimalSeparator: string = new Intl.NumberFormat(locale)
        .format(1.1)
        .substring(1, 2);
      // try to get gradeScale from cache map
      let gradeScale: GradeScale = this._gradeScaleCache.get(
        calculationAttributes.graduationGradeScale.id
      );
      if (gradeScale == null) {
        // get grade scale with service
        gradeScale = await this._gradeScaleService.getGradeScale(
          calculationAttributes.graduationGradeScale.id
        );
        // and cache it
        this._gradeScaleCache.set(
          calculationAttributes.graduationGradeScale.id,
          gradeScale
        );
      }
      // get formatted grade
      const formattedGrade: string = gradeScale.format(
        calculationAttributes.graduationGrade
      );
      // and calculate grade written in words
      const graduationGradeWrittenInWords: string =
        scaleType === 0
          ? formatter
              .format(
                parseFloat(formattedGrade.replace(decimalSeparator, '.')),
                locale,
                decimalDigits
              )
              // ΝΟΤΕ: toLocaleUpperCase(locale) could be used here, but the current version of ts does not accept it.
              .toLocaleUpperCase()
              .normalize('NFD')
              .replace(/[\u0300-\u036f]/g, '')
          : formattedGrade;
      return graduationGradeWrittenInWords;
    } catch (err) {
      console.error(err);
      throw err;
    }
  }

  async checkStudentRemarkGrades(studentId: number) {
    let items = [];
    // get remarked passed grades
    const remarks = await this._context.model('StudentGrades').where('student')
      .equal(studentId)
      .and('status/alternateName').equal('remark')
      .and('grade2').greaterOrEqual({'$name': '$it/courseExam/course/gradeScale/scaleBase'})
      .select('courseExam/course as course')
      .take(-1)
      .getItems();
    if (remarks && remarks.length > 0) {
      // and check if course is not passed
      const notPassedCourses = await this._context.model('StudentCourses').where('student')
        .equal(studentId)
        .and('isPassed').notEqual(1)
        .expand('course')
        .take(-1)
        .getItems();
      items = notPassedCourses.filter(item => {
        return remarks.findIndex((x) => {
          return x.course === item.course.id;
        }) >= 0;
      });
    }
    return items;
  }

}
