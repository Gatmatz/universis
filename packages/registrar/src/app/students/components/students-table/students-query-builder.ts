export const properties: {
    name: string,
    label: string,
    type?: string;
    source?: string,
    template?: string
}[] = [{
    name: 'inscriptionMode',
    label: 'Students.InscriptionMode'
}, {
    name: 'inscriptionYear',
    label: 'Students.inscriptionYear',
    source: '/AcademicYears?$top=-1&$select=id as raw,name as label&$orderby=id desc'
},{
    name: 'departmentSnapshot',
    type: 'DepartmentSnapshot',
    label: 'Students.AddDepartmentSnapshot',
    source: '/DepartmentSnapshots?$top=-1&$select=id as raw,description as label&$orderby=name desc&$filter=department eq {{form.customParams.currentDepartment}}'
}, {
    name: 'inscriptionPeriod',
    label: 'Students.inscriptionPeriod'
}, {
    name: 'category',
    label: 'Students.StudentCategory'
}, {
    name: 'declaredDate',
    label: 'Students.DeclaredDate'
}, {
    name: 'graduationDate',
    label: 'Students.GraduationDate'
}, {
    name: 'graduationPeriod',
    label: 'Students.GraduationPeriod'
}, {
    name: 'graduationYear',
    label: 'Students.GraduationYear',
    source: '/AcademicYears?$top=-1&$select=id as raw,name as label&$orderby=id desc'
}, {
    name: 'graduationType',
    label: 'Students.GraduationType'
}, {
    name: 'inscriptionDate',
    label: 'Students.InscriptionDate'
}, {
    name: 'inscriptionModeCategory',
    label: 'Students.InscriptionModeCategory'
}, {
    name: 'removalDate',
    label: 'Students.RemovalDate'
}, {
    name: 'removalPeriod',
    label: 'Students.RemovalPeriod'
}, {
    name: 'removalYear',
    label: 'Students.RemovalYear',
    source: '/AcademicYears?$top=-1&$select=id as raw,name as label&$orderby=id desc'
}, {
    name: 'semester',
    label: 'Students.Semester'
}, {
    name: 'studentIdentifier',
    label: 'Students.StudentIdentifier.short'
}, {
    name: 'studyProgram',
    label: 'Students.studyProgram',
    source: '/StudyPrograms?$top=-1&$select=id as raw,name as label&$orderby=name desc&$filter=department eq {{form.customParams.currentDepartment}}'
}, {
    name: 'studyProgramSpecialty',
    label: 'Students.Specialty',
    template: '{{ item.studyProgram }} > {{ item.label }}',
    source: '/StudyProgramSpecialties?$top={{ limit }}&$skip={{ skip }}&$orderby=studyProgram/name,name&$filter=studyProgram/department eq {{form.customParams.currentDepartment}}&$select=id as raw,name as label,studyProgram/name as studyProgram'
}, {
    name: 'uniqueIdentifier',
    label: 'Students.uniqueIdentifier'
}, {
    name: 'studentStatus',
    label: 'Students.studentStatus',
    source: '/StudentStatuses?$top=-1&$select=id as raw,name as label&$orderby=id desc'
},{
    name: 'user',
    label: 'Students.User',
    source: '/Users?$top={{limit}}&$skip={{skip}}&$select=id as raw,name as label&$filter=indexof(name, \'{{encodeURIComponent(search||\'\')}}\') ge 0'
}, {
    name: 'user/name',
    type: 'Text',
    label: 'Students.UserName'
}, {
    name: 'person/familyName',
    label: 'Students.FamilyName'
}, {
    name: 'person/givenName',
    label: 'Students.GivenName'
}, {
    name: 'person/SSN',
    label: 'Students.SSN'
}, {
    name: 'person/birthDate',
    label: 'Students.BirthDate'
}, {
    name: 'person/birthPlace',
    label: 'Students.BirthPlace'
}, {
    name: 'person/familyStatus',
    label: 'Students.FamilyStatus'
}, {
    name: 'person/fatherName',
    label: 'Students.FatherName'
}, {
    name: 'person/gender',
    label: 'Students.Gender',
    source: '/Genders?$select=identifier as raw,name as label'
}, {
    name: 'person/homeAddress',
    label: 'Students.HomeAddress'
}, {
    name: 'person/homeCity',
    label: 'Students.HomeCity'
}, {
    name: 'person/homeCountry',
    label: 'Students.HomeCountry'
}, {
    name: 'person/homePhone',
    label: 'Students.HomePhone'
}, {
    name: 'person/mobilePhone',
    label: 'Students.MobilePhone'
}, {
    name: 'person/motherName',
    label: 'Students.MotherName'
}, {
    name: 'person/nationality',
    label: 'Students.Nationality'
},{
    name: 'person/temporaryAddress',
    label: 'Students.TemporaryAddress'
}, {
    name: 'person/temporaryCity',
    label: 'Students.TemporaryCity'
}, {
    name: 'person/temporaryCountry',
    label: 'Students.TemporaryCountry'
}, {
    name: 'person/temporaryPhone',
    label: 'Students.TemporaryPhone'
},{
    name: 'person/vatNumber',
    label: 'Students.VatNumber'
}
];

export const sharedProperties: {
    name: string,
    label: string,
    type?: string;
    source?: string,
    template?: string
}[] = [{
    name: 'inscriptionMode',
    label: 'Students.InscriptionMode'
}, {
    name: 'inscriptionYear',
    label: 'Students.inscriptionYear',
    source: '/AcademicYears?$top=-1&$select=id as raw,name as label&$orderby=id desc'
},{
    name: 'departmentSnapshot',
    type: 'DepartmentSnapshot',
    label: 'Students.AddDepartmentSnapshot',
    source: '/DepartmentSnapshots?$top=-1&$select=id as raw,description as label&$orderby=name desc&$filter=department eq {{form.customParams.currentDepartment}}'
}, {
    name: 'inscriptionPeriod',
    label: 'Students.inscriptionPeriod'
}, {
    name: 'category',
    label: 'Students.StudentCategory'
}, {
    name: 'declaredDate',
    label: 'Students.DeclaredDate'
}, {
    name: 'graduationDate',
    label: 'Students.GraduationDate'
}, {
    name: 'graduationPeriod',
    label: 'Students.GraduationPeriod'
}, {
    name: 'graduationYear',
    label: 'Students.GraduationYear',
    source: '/AcademicYears?$top=-1&$select=id as raw,name as label&$orderby=id desc'
}, {
    name: 'graduationType',
    label: 'Students.GraduationType'
}, {
    name: 'inscriptionDate',
    label: 'Students.InscriptionDate'
}, {
    name: 'inscriptionModeCategory',
    label: 'Students.InscriptionModeCategory'
}, {
    name: 'removalDate',
    label: 'Students.RemovalDate'
}, {
    name: 'removalPeriod',
    label: 'Students.RemovalPeriod'
}, {
    name: 'removalYear',
    label: 'Students.RemovalYear',
    source: '/AcademicYears?$top=-1&$select=id as raw,name as label&$orderby=id desc'
}, {
    name: 'semester',
    label: 'Students.Semester'
}, {
    name: 'studentIdentifier',
    label: 'Students.StudentIdentifier.short'
}, {
    name: 'studyProgram',
    label: 'Students.studyProgram',
    source: '/StudyPrograms?$top=-1&$select=id as raw,name as label&$orderby=name desc&$filter=department eq {{form.customParams.currentDepartment}}'
}, {
    name: 'studyProgramSpecialty',
    label: 'Students.Specialty',
    template: '{{ item.studyProgram }} > {{ item.label }}',
    source: '/StudyProgramSpecialties?$top={{ limit }}&$skip={{ skip }}&$orderby=studyProgram/name,name&$filter=studyProgram/department eq {{form.customParams.currentDepartment}}&$select=id as raw,name as label,studyProgram/name as studyProgram'
}, 
{
    name: 'studentStatus',
    label: 'Students.studentStatus',
    source: '/StudentStatuses?$top=-1&$select=id as raw,name as label&$orderby=id desc'
},{
    name: 'uniqueIdentifier',
    label: 'Students.uniqueIdentifier'
}, {
    name: 'user',
    label: 'Students.User',
    source: '/Users?$top={{limit}}&$skip={{skip}}&$select=id as raw,name as label&$filter=indexof(name, \'{{encodeURIComponent(search||\'\')}}\') ge 0'
}, {
    name: 'user/name',
    type: 'Text',
    label: 'Students.UserName'
}, {
    name: 'person/familyName',
    label: 'Students.FamilyName'
}, {
    name: 'person/givenName',
    label: 'Students.GivenName'
}, {
    name: 'person/SSN',
    label: 'Students.SSN'
}, {
    name: 'person/birthDate',
    label: 'Students.BirthDate'
}, {
    name: 'person/birthPlace',
    label: 'Students.BirthPlace'
}, {
    name: 'person/familyStatus',
    label: 'Students.FamilyStatus'
}, {
    name: 'person/fatherName',
    label: 'Students.FatherName'
}, {
    name: 'person/gender',
    label: 'Students.Gender',
    source: '/Genders?$select=identifier as raw,name as label'
}];