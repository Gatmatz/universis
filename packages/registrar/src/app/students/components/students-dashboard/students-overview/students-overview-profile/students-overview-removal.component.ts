import {Component, OnInit, Input, OnDestroy, SimpleChanges} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-students-overview-removal',
  templateUrl: './students-overview-removal.component.html'
})
export class StudentsOverviewRemovalComponent implements OnInit, OnDestroy  {

  public student;
  @Input() studentId: number;
  private subscription: Subscription;
  private fragmentSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) { }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.studentId) {
      if (changes.studentId.currentValue == null) {
        this.student = null;
        return;
      }
      this.fetchData(changes.studentId.currentValue).then(student => {
        this.student = student;
      }).catch(err => {
        // log error
        console.error(err);
      });
    }
  }


  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.student = await this._context.model('Students')
        .where('id').equal(this.studentId)
        .expand('person($expand=gender), department, studyProgram, user')
        .getItem();
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          if (this.studentId) {
            this.fetchData(this.studentId).then(student => {
              this.student = student;
            }).catch(err => {
              console.error(err);
            });
          }
        }
      });
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  fetchData(studentId: number | string): Promise<any> {
    return new Promise((resolve, reject) => {
      return this._context.model('Students')
        .where('id').equal(studentId)
        .expand('person($expand=gender), department, studyProgram, user')
        .getItem().then(student => {
          // resolve student
          return resolve(student);
        }).catch(err => {
          // reject with error
          return reject(err);
        });
    });
  }
}
