import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { TableConfiguration } from '@universis/ngx-tables';
import { cloneDeep } from 'lodash';
import { ReferrerRouteService, TemplatePipe } from '@universis/common';
import { Subscription } from 'rxjs';
import { TableConfigurationResolver } from '../../../registrar-shared/table-configuration.resolvers';

@Component({
  selector: 'app-instructors-root',
  templateUrl: './instructors-root.component.html',
  styleUrls: ['./instructors-root.component.scss']
})
export class InstructorsRootComponent implements OnInit, OnDestroy {
  public model: any;
  public instructorId: any;
  public actions: any[];
  public allowedActions: any[];
  public edit: any;
  referrerSubscription: Subscription;
  paramSubscription: Subscription;
  referrerRoute: any = {
    queryParams: {},
    commands: []
  };

  constructor(private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _context: AngularDataContext,
    private _template: TemplatePipe,
    private _resolver: TableConfigurationResolver,
    public referrer: ReferrerRouteService) {
  }
  ngOnDestroy(): void {
    if (this.referrerSubscription) {
      this.referrerSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  ngOnInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe((params: { id?: string }) => {
      this.instructorId = params.id;
      this._context.model('Instructors')
        .where('id').equal(params.id)
        .expand('department')
        .getItem().then((data) => {
          this.model = data;
          this._resolver.get('Instructors').subscribe((config: TableConfiguration) => {

            // get referrer
            this.referrerSubscription = this.referrer.routeParams$.subscribe(result => {
              const pathConfiguration = config as { paths?: { name: string, alternateName: string }[] };
              let redirectCommands = [
                '/', 'instructors'
              ]
              if (pathConfiguration.paths && pathConfiguration.paths.length) {
                const addCommands = pathConfiguration.paths[0].alternateName.split('/');
                redirectCommands = [
                  '/', 'instructors'
                ].concat(addCommands);
              }
              this.referrerRoute.commands = redirectCommands;
            });

            if (config.columns && this.model) {
              // get actions from config file
              this.actions = config.columns.filter((x: any) => {
                return x.actions;
              })
                // map actions
                .map((x: any) => x.actions)
                // get list items
                .reduce((a: any, b: any) => b, 0);

              // filter actions with student permissions
              this.allowedActions = this.actions.filter(x => {
                if (x.role) {
                  if (x.role === 'action') {
                    return x;
                  }
                }
              });

              this.edit = this.actions.find(x => {
                if (x.role === 'edit') {
                  x.href = this._template.transform(x.href, this.model);
                  return x;
                }
              });
              this.actions = this.allowedActions;
              this.actions.forEach(action => {
                action.href = this._template.transform(action.href, this.model);
              });
            }
          });
        });
    });

  }
}
