import { Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
import { ActivatedTableService } from '@universis/ngx-tables';
import { Subscription, combineLatest } from 'rxjs';
import { DIALOG_BUTTONS, ErrorService, ModalService, ToastService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';

@Component({
  selector: 'app-instructors-dashboard-classes',
  templateUrl: './instructors-dashboard-classes.component.html',
  styleUrls: ['../instructors-dashboard.component.scss']
})
export class InstructorsDashboardClassesComponent implements OnInit, OnDestroy {

  public recordsTotal: any;
  private dataSubscription: Subscription;
  @ViewChild('classes') classes: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  instructorID: any = this._activatedRoute.snapshot.params.id;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _activatedTable: ActivatedTableService,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _translateService: TranslateService,
    private _toastService: ToastService
  ) {
    //
  }

  ngOnInit() {


    this.dataSubscription = combineLatest(this._activatedRoute.params, this._activatedRoute.data)
      .subscribe((results) => {
          const params = results[0];
          const data = results[1];
          this._activatedTable.activeTable = this.classes;
          this.classes.query = this._context.model('courseClassInstructors')
            .where('instructor').equal(params.id)
            .expand('courseClass($expand=course,year,period)')
            .orderByDescending('courseClass/year')
            .thenByDescending('courseClass/period')
            .prepare();
          const loaded = this.tableConfiguration != null;
          this.classes.config = this.tableConfiguration = data.tableConfiguration;
          if (loaded) {
            this.classes.fetch(true);
          }
          this.search.form = this.searchConfiguration = data.searchConfiguration;
      });

    // do reload by using hidden fragment e.g. /classes#reload
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.classes.fetch(true);
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  remove() {
    if (this.classes && this.classes.selected && this.classes.selected.length) {
      // get items to remove
      const items = this.classes.selected.map(item => {
        return {
          instructor: this.instructorID,
          courseClass: item.id
        };
      });
      return this._modalService.showWarningDialog(
        this._translateService.instant('Instructors.RemoveClassesTitle'),
        this._translateService.instant('Instructors.RemoveClassesMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this._context.model('CourseClassInstructors').remove(items).then(() => {
              this._toastService.show(
                this._translateService.instant('Instructors.RemoveClassMessage.title'),
                this._translateService.instant((items.length === 1 ?
                  'Instructors.RemoveClassMessage.one' : 'Instructors.RemoveClassMessage.many')
                  , { value: items.length })
              );
              this.classes.fetch(true);
            }).catch(err => {
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });

    }
  }
}
