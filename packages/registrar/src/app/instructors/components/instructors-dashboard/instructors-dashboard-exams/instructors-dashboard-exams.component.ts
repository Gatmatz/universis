import { Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
import { Subscription, combineLatest } from 'rxjs';
import { DIALOG_BUTTONS, ErrorService, ModalService, ToastService } from '@universis/common';
import { ActivatedTableService } from '@universis/ngx-tables';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';


@Component({
  selector: 'app-instructors-dashboard-exams',
  templateUrl: './instructors-dashboard-exams.component.html',
  styleUrls: ['../instructors-dashboard.component.scss']
})
export class InstructorsDashboardExamsComponent implements OnInit, OnDestroy {

  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  private dataSubscription: Subscription;
  public instructorID: any;
  @ViewChild('exams') exams: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;

  constructor(private _activatedRoute: ActivatedRoute,
    private _translateService: TranslateService,
    private _activatedTable: ActivatedTableService,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _toastService: ToastService,
    private _context: AngularDataContext) { }

  ngOnInit() {

    this.dataSubscription = combineLatest(this._activatedRoute.params, this._activatedRoute.data)
      .subscribe((results) => {
          const params = results[0];
          this.instructorID = params.id;
          const data = results[1];
          this._activatedTable.activeTable = this.exams;
          this.exams.query = this._context.model('CourseExamInstructors')
            .where('instructor').equal(params.id)
            .expand('courseExam($expand=course,examPeriod)')
            .orderByDescending('courseExam/year')
            .thenByDescending('courseExam/examPeriod')
            .prepare();
          const loaded = this.tableConfiguration != null;
          this.exams.config = this.tableConfiguration = data.tableConfiguration;
          if (loaded) {
            this.exams.fetch(true);
          }
          this.search.form = this.searchConfiguration = data.searchConfiguration;
      });

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.exams.fetch(true);
        }
      });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }
  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  remove() {
    if (this.exams && this.exams.selected && this.exams.selected.length) {
      const items = this.exams.selected.map(item => {
        return {
          instructor: this.instructorID,
          courseExam: item.id
        };
      });
      return this._modalService.showWarningDialog(
        this._translateService.instant('Instructors.RemoveExamsTitle'),
        this._translateService.instant('Instructors.RemoveExamMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this._context.model('CourseExamInstructors').remove(items).then(() => {
              this._toastService.show(
                this._translateService.instant('Instructors.RemoveExamsMessage.title'),
                this._translateService.instant((items.length === 1 ?
                  'Instructors.RemoveExamsMessage.one' : 'Instructors.RemoveExamsMessage.many')
                  , { value: items.length })
              );
              this.exams.fetch(true);
            }).catch(err => {
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });

    }
  }

}
