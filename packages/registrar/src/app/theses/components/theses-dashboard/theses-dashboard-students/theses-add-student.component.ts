import {Component, Directive, EventEmitter, Input, OnDestroy, OnInit} from '@angular/core';
// tslint:disable-next-line:max-line-length
import {AdvancedTableModalBaseComponent, AdvancedTableModalBaseTemplate} from '@universis/ngx-tables';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';
import {AppEventService, ErrorService, LoadingService, ToastService} from '@universis/common';
import {AdvancedFilterValueProvider} from '@universis/ngx-tables';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-theses-add-student',
  template: AdvancedTableModalBaseTemplate
})
export class ThesesAddStudentComponent extends AdvancedTableModalBaseComponent {
  @Input() thesis: any;

  constructor(_router: Router, _activatedRoute: ActivatedRoute,
              protected _context: AngularDataContext,
              private _errorService: ErrorService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
              private _loadingService: LoadingService,
              private _advancedFilterValueProvider: AdvancedFilterValueProvider,
              private _appEvent: AppEventService,
              private _datePipe: DatePipe) {
    super(_router, _activatedRoute, _context, _advancedFilterValueProvider, _datePipe);
    // set default title
    this.modalTitle = 'Theses.AddStudent';
    this.tableConfigSrc = 'assets/tables/Students/config.active.json';
  }
  hasInputs(): Array<string> {
    return [ 'thesis' ];
  }
  ok(): Promise<any> {
    this._loadingService.showLoading();
    const selected = this.advancedTable.selected;
    let items = [];
    if (selected && selected.length > 0) {
      items = selected.map( student => {
        return {
          thesis: this.thesis,
          student: student
        };
      });
      return this._context.model('StudentTheses')
        .save(items)
        .then( result => {
          this._loadingService.hideLoading();
          // add toast message
          this._toastService.show(
            this._translateService.instant('Theses.AddStudentsMessage.title'),
            this._translateService.instant((items.length === 1 ?
              'Theses.AddStudentsMessage.one' : 'Theses.AddStudentsMessage.many')
              , { value: items.length })
          );
          this._appEvent.change.next({
            model: 'Theses',
            target: this.thesis.id
          });
          return this.close({
            fragment: 'reload',
            skipLocationChange: true
          });
        }).catch( err => {
          this._loadingService.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
    }
    return this.close();
  }
}
