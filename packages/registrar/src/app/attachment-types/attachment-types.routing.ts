import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AttachmentTypesRootComponent} from './components/attachment-types-root/attachment-types-root.component';
import {AttachmentTypesHomeComponent} from './components/attachment-types-home/attachment-types-home.component';
import {AttachmentTypesTableComponent} from './components/attachment-types-table/attachment-types-table.component';
import {AttachmentTypesOverviewComponent} from './components/attachment-types-dashboard/attachment-types-overview/attachment-types-overview.component';
import {AttachmentTypesDashboardComponent} from './components/attachment-types-dashboard/attachment-types-dashboard.component';
import {AdvancedFormRouterComponent} from '@universis/forms';

const routes: Routes = [
  {
    path: '',
    component: AttachmentTypesHomeComponent,
    data: {
      title: 'AttachmentTypes'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list'
      },
      {
        path: 'list',
        component: AttachmentTypesTableComponent,
        data: {
          title: 'AttachmentTypes List'
        }
      }
    ]
  },
  {
    path: 'create',
    component: AttachmentTypesRootComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'new'
      },
      {
        path: 'new',
        component: AdvancedFormRouterComponent
      }
    ],
    resolve: {

    }
  },
  {
    path: ':id',
    component: AttachmentTypesRootComponent,
    data: {
      title: 'AttachmentTypes Root'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
      },
      {
        path: 'dashboard',
        component: AttachmentTypesDashboardComponent,
        data: {
          title: 'AttachmentTypes.Overview'
        },
        children: [
          {
            path: '',
            redirectTo: 'overview',
            pathMatch: 'full'
          },
          {
            path: 'overview',
            component: AttachmentTypesOverviewComponent,
            data: {
              title: 'AttachmentTypes.Overview'
            }
          }
        ]
      },
      {
        path: ':action',
        component: AdvancedFormRouterComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AttachmentTypesRoutingModule {
}
