import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalsDoughnutComponent } from './modals-doughnut.component';

describe('ModalsDoughnutComponent', () => {
  let component: ModalsDoughnutComponent;
  let fixture: ComponentFixture<ModalsDoughnutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalsDoughnutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalsDoughnutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
