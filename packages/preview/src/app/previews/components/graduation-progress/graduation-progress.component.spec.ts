import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduationProgressComponent } from './graduation-progress.component';

describe('GraduationProgressComponent', () => {
  let component: GraduationProgressComponent;
  let fixture: ComponentFixture<GraduationProgressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduationProgressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduationProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
